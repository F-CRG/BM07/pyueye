#======================================================================
# THIS SCRIPT READS UEYE CAMERAS
# IT IS POSSIBLE TO SCHEDULE MULTIPLE ACQUISITIONS, IN LOOP OR AT SCHEDULED TIMES
#
# AUTHOR: Felix Billaud, Dec. 2022
# LAST MODIFICATION: Eric Rive-Mathieu, Jan. 2023
#======================================================================
from pypyueye import Camera, FrameThread, SaveThread, RecordThread, \
    PyuEyeQtApp, PyuEyeQtView, UselessThread, CircleDetector
from pyueye import ueye
import matplotlib.pyplot as plt
import time
import datetime
from datetime import datetime, timedelta
import tzlocal
from datetime import date
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.schedulers.background import BackgroundScheduler
import os


#======================================================================
# VARIABLES TO EDIT
#======================================================================

#MODE SELECTION
mode="single" #Choose the mode between: 'single', 'multiple', 'scheduled', 'live'

#FILE NAME
file="C:/Users/ermathie/Work Folders/Documents/tmp/videotestthrowaway{}_{}FPS.avi"      #Savefile  {} is to be left in for date and time of run in file name (if needed)

#CAMERA SETTINGS (CHECK POSSIBILITIES WITH UEYE COCKPIT SOFTWARE)
FPS=20  #INT
nb_frames = 10  #Nmb of frames to acquire for each run
Buffer= nb_frames+500     # Memory bufer frame size
w=400                     #Width in pixels
h=400                   #Height in pixels
x=0                         #x position of the AOI          Positions don't refer to center of AOI but to upper left corner of it
y=0                         #y position of AOI              Just copy the ueye values as they are

#MULTIPLE ACQUISITONS
nb_acq = 2 #nb of acquisitions
sleep = 30 #seconds between 2 acquisitions

#SCHEDULED
nb_acq=2                    #number of runs to schedule
Delta=60                    #Interval between two runs in seconds
rundate=datetime(2023, 1, 11,14,33,00)   #heure de la première acquisition Y,M,D,H,Min,S

#======================================================================
# END VARIABLES TO BE EDITED
#======================================================================





def live_video():
    with Camera(device_id=0, buffer_count=Buffer) as cam:  # buffer_count>=nmb_frames
        #======================================================================
        # Camera settings
        #======================================================================

        # cam.set_colormode(ueye.IS_CM_MONO8)   #IS_CM_BGR8_PACKED for color  and    IS_CM_MONO8 / ueye.IS_CM_RGBY8_PACKED for BW (change bytes per pixel accordingly)
        cam.set_colormode(ueye.IS_CM_BGR8_PACKED)
        #
        cam.set_aoi(x, y, w, h)
        print(f"INITIAL VALUES")
        print(f'fps: {cam.get_fps()}')
        print(f'Available fps range: {cam.get_fps_range()}')
        print(f'Pixelclock: {cam.get_pixelclock()}')
        cam.set_pixelclock(128)  
        cam.set_fps(FPS)
        # cam.set_exposure(6.235)        #laisser auto
        cam.set_exposure_auto(1)
        
        cam.set_gain_auto(0)
        hCam = ueye.HIDS(0)
        ueye.is_SetGlobalShutter(hCam, ueye.IS_DEVICE_FEATURE_CAP_SHUTTER_MODE_GLOBAL)
        # nShutterMode = ueye.IS_DEVICE_FEATURE_CAP_SHUTTER_MODE_ROLLING
        # ueye.is_DeviceFeature(hCam, ueye.IS_DEVICE_FEATURE_CMD_SET_SHUTTER_MODE, ueye.IS_DEVICE_FEATURE_CAP_SHUTTER_MODE_GLOBAL,8 )
      
        # mode=ueye.DEVICE_FEATURE_CMD(7)
        
        print("shuttermode is :")
        # print(mode)
        print("")
        print(f"MODIFIED VALUES")
        print(f'fps: {cam.get_fps()}')
        print(f'Available fps range: {cam.get_fps_range()}')
        print(f'Pixelclock: {cam.get_pixelclock()}')
        
        a = time.time()
        nmb_ims = FPS
        ims = cam.capture_images(nmb_ims)
        b=time.time()
        fps = cam.get_fps()
        fpsr = cam.get_fps_range()

        print("")
        print(f"AFTER CAPTURE VALUES")
        print(f"Framerate: {fps}")
        print(f'Measured framerate: {nmb_ims/(b - a)}') #measured FPS considerably undersells the real FPS
        print(f"Frame range: {fpsr[0]}, {fpsr[1]}")
        # qt only deals with color images ?
        cam.set_colormode(ueye.IS_CM_BGR8_PACKED)
    
       
        app = PyuEyeQtApp()
     
        view = PyuEyeQtView()
        view.show()
    
        thread = FrameThread(cam, view)
        thread.start()
        app.exit_connect(thread.stop)

        app.exec_()
        
        
        

def record(FPS,nb_frames,Buffer,w,h,x,y,file):
    with Camera(device_id=0, buffer_count=Buffer) as cam:  # buffer_count>=nmb_frames
        #======================================================================
        # Camera settings
        #======================================================================

        # cam.set_colormode(ueye.IS_CM_MONO8)   #IS_CM_BGR8_PACKED for color  and    IS_CM_MONO8 / ueye.IS_CM_RGBY8_PACKED for BW (change bytes per pixel accordingly)
        cam.set_colormode(ueye.IS_CM_BGR8_PACKED)
        #
        cam.set_aoi(x, y, w, h)
        print(f"INITIAL VALUES")
        print(f'fps: {cam.get_fps()}')
        print(f'Available fps range: {cam.get_fps_range()}')
        print(f'Pixelclock: {cam.get_pixelclock()}')
        cam.set_pixelclock(128)  
        cam.set_fps(FPS)
        # cam.set_exposure(6.235)        #laisser auto
        cam.set_exposure_auto(1)
        
        cam.set_gain_auto(0)
        hCam = ueye.HIDS(0)
        ueye.is_SetGlobalShutter(hCam, ueye.IS_DEVICE_FEATURE_CAP_SHUTTER_MODE_GLOBAL)
        # nShutterMode = ueye.IS_DEVICE_FEATURE_CAP_SHUTTER_MODE_ROLLING
        # ueye.is_DeviceFeature(hCam, ueye.IS_DEVICE_FEATURE_CMD_SET_SHUTTER_MODE, ueye.IS_DEVICE_FEATURE_CAP_SHUTTER_MODE_GLOBAL,8 )
      
        # mode=ueye.DEVICE_FEATURE_CMD(7)
        
        print("shuttermode is :")
        # print(mode)
        print("")
        print(f"MODIFIED VALUES")
        print(f'fps: {cam.get_fps()}')
        print(f'Available fps range: {cam.get_fps_range()}')
        print(f'Pixelclock: {cam.get_pixelclock()}')

        #==============================================================================
        # Capturing images in memory
        #==============================================================================
   
        a = time.time()
        nmb_ims = FPS
        ims = cam.capture_images(nmb_ims)
        b=time.time()
        fps = cam.get_fps()
        fpsr = cam.get_fps_range()

        print("")
        print(f"AFTER CAPTURE VALUES")
        print(f"Framerate: {fps}")
        print(f'Measured framerate: {nmb_ims/(b - a)}') #measured FPS considerably undersells the real FPS
        print(f"Frame range: {fpsr[0]}, {fpsr[1]}")

        # plt.figure()
        # plt.imshow(ims[-1])
        # plt.show()
        cam.set_pixelclock(128)
        cam.set_aoi(x, y, h,w)
        cam.set_fps(FPS)            
        # cam.set_exposure(5.76)
        cam.set_exposure_auto(1)        # all auto parameter functions work with 0 for off and 1 for on
        cam.set_gain_auto(0)
        nmb_frames = nb_frames
        tmps=str(datetime.now()).replace(":","_")
        filepath= file.format(tmps,FPS)  
        # wait = input("PRESS ENTER TO BEGIN RECORDING.")
        cam.set_colormode(ueye.IS_CM_MONO8) 
        # cam.set_colormode(ueye.IS_CM_BGR8_PACKED)
        # Create a thread to save a video
        thread = RecordThread(cam, path=filepath,
                              use_memory=True,
                              nmb_frame=nmb_frames,
                              verbose=True)
        # test1=datetime.now()
        thread.start()
        # Wait for the thread to edn
        thread.join()
        thread.stop()
        # print(datetime.now()-test1)



if __name__ == "__main__":
    if mode == 'single':
         record(FPS,nb_frames,Buffer,h,w,x,y,file)
    elif mode == "multiple":
         for i in range(nb_acq):
             record(FPS,nb_frames,Buffer,h,w,x,y,file)     
             time.sleep(sleep)
    elif mode == "scheduled":
        sched1 = BackgroundScheduler(timezone=str(tzlocal.get_localzone()))
        sched1.start()
        heure=rundate
        for i in range (nb_acq):
            sched1.add_job(record, 'date', run_date=heure, args=[FPS,nb_frames,Buffer,h,w,x,y,file])
            heure=heure+timedelta(0,Delta)  #Delta en secondes
        print ('The video acquisition has been scheduled and will automatically start at the follwing times:')
        sched1.print_jobs()
    else:
        live_video()
        