# pyueye

## Name
Ueye camera pictures acquisition

## Description
This programs allows to control a ueye camera with python. We use it to monitor our X-ray beam at high frequency and check the beam vibration frequencies.

The output is in a .avi format. To analyse the data, use ffmpeg to generate individual pictures: ffmpeg -i test.avi image%d.png

Alternatively, use the python script available in the root directory of this project.

For frequency analyses, see the project:
https://gitlab.esrf.fr/F-CRG/BM07/scripts-jupyter-shadow/picture-and-video-analyses/-/blob/main/video_beam_noise_analysis-centroid.ipynb

fqfdsf
