# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 10:58:01 2023

@author: ERMATHIE

Program to convert an avi file into multiple png pictures.
"""

# Importing all necessary libraries
import cv2
import os
  
# Read the video from specified path
cam = cv2.VideoCapture("D:\\pypyueye\\Videos\\videotestglobal_2_2022-12-22 11_08_04.679195.avi")
  
try:
      
    # creating a folder named data
    if not os.path.exists('pictures'):
        os.makedirs('pictures')
  
# if not created then raise error
except OSError:
    print ('Error: Creating directory of data')
  
# frame
currentframe = 0
  
while(True):
      
    # reading from frame
    ret,frame = cam.read()
  
    if ret:
        # if video is still left continue creating images
        name = './pictures/frame' + str(currentframe) + '.png'
        print ('Creating...' + name)
  
        # writing the extracted images
        cv2.imwrite(name, frame)
  
        # increasing counter so that it will
        # show how many frames are created
        currentframe += 1
    else:
        break
  
# Release all space and windows once done
cam.release()
cv2.destroyAllWindows()